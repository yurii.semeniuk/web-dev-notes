# Web Dev Notes

Let's get it started!

My name is **Yurii Semeniuk** and I'm a *full stack web developer* for over 10 years. I will post some interesting solutions regarding web development here: https://t.me/web_dev_notes

My website: [ysemeniuk.com](http://ysemeniuk.com)

#helloworld #webdevnotes